# automatically generated from mdmp.vs2

from libvstruct2 import BM8, DynExpr, FILETIME, I1, I2, I4, I4UT, I8, R, VStruct, WCHAR


from .Windows import PRIORITY_CLASS


class MINIDUMP_LOCATION_DESCRIPTOR64(VStruct):
    SFORMAT = 'QQ'
    SOFFSETS = [0, 8, 16]
    ENDIANNESS = '<'
    FIELDS = [
        {'name': 'DataSize', 'type': I8},
        {'name': 'Rva', 'type': I8},
    ]


class MINIDUMP_LOCATION_DESCRIPTOR(VStruct):
    SFORMAT = 'II'
    SOFFSETS = [0, 4, 8]
    ENDIANNESS = '<'
    FIELDS = [
        {'name': 'DataSize', 'type': I4},
        {'name': 'Rva', 'type': I4},
    ]


class MINIDUMP_MEMORY_DESCRIPTOR64(VStruct):
    SFORMAT = 'QQ'
    SOFFSETS = [0, 8, 16]
    ENDIANNESS = '<'
    FIELDS = [
        {'name': 'StartOfMemoryRange', 'type': I8},
        {'name': 'DataSize', 'type': I8},
    ]


class MINIDUMP_MEMORY_DESCRIPTOR(VStruct):
    FIELDS = [
        {'name': 'StartOfMemoryRange', 'type': I8},
        {'name': 'Memory', 'type': MINIDUMP_LOCATION_DESCRIPTOR},
    ]


class MINIDUMP_STRING(VStruct):
    FIELDS = [
        {'name': 'Length', 'type': I4},
        {'name': 'Buffer', 'type': WCHAR, 'length': DynExpr('$Length // 2')},
    ]


class VS_FIXEDFILEINFO(VStruct):
    SFORMAT = 'IIIIIIIIIIIII'
    SOFFSETS = [0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52]
    ENDIANNESS = '<'
    FIELDS = [
        {'name': 'dwSignature', 'type': I4},
        {'name': 'dwStrucVersion', 'type': I4},
        {'name': 'dwFileVersionMS', 'type': I4},
        {'name': 'dwFileVersionLS', 'type': I4},
        {'name': 'dwProductVersionMS', 'type': I4},
        {'name': 'dwProductVersionLS', 'type': I4},
        {'name': 'dwFileFlagsMask', 'type': I4},
        {'name': 'dwFileFlags', 'type': I4},
        {'name': 'dwFileOS', 'type': I4},
        {'name': 'dwFileType', 'type': I4},
        {'name': 'dwFileSubtype', 'type': I4},
        {'name': 'dwFileDateMS', 'type': I4},
        {'name': 'dwFileDateLS', 'type': I4},
    ]


class MINIDUMP_THREAD_INFO(VStruct):
    SFORMAT = 'IIIIQQQQQQ'
    SOFFSETS = [0, 4, 8, 12, 16, 24, 32, 40, 48, 56, 64]
    ENDIANNESS = '<'
    FIELDS = [
        {'name': 'ThreadId', 'type': I4},
        {'name': 'DumpFlags', 'type': I4},
        {'name': 'DumpError', 'type': I4},
        {'name': 'ExitStatus', 'type': I4},
        {'name': 'CreateTime', 'type': FILETIME},
        {'name': 'ExitTime', 'type': FILETIME},
        {'name': 'KernelTime', 'type': FILETIME},
        {'name': 'UserTime', 'type': FILETIME},
        {'name': 'StartAddress', 'type': I8},
        {'name': 'Affinity', 'type': I8},
    ]


class MINIDUMP_THREAD_INFO_LIST(VStruct):
    FIELDS = [
        {'name': 'SizeOfHeader', 'type': I4},
        {'name': 'SizeOfEntry', 'type': I4},
        {'name': 'NumberOfEntries', 'type': I4},
        {'name': 'Entries', 'count': DynExpr('$NumberOfEntries'), 'type': MINIDUMP_THREAD_INFO},
    ]


class MINIDUMP_THREAD(VStruct):
    FIELDS = [
        {'name': 'ThreadId', 'type': I4},
        {'name': 'SuspendCount', 'type': I4},
        {'name': 'PriorityClass', 'type': I4, 'formatter': PRIORITY_CLASS},
        {'name': 'Priority', 'type': I4},
        {'name': 'Teb', 'type': I8},
        {'name': 'Stack', 'type': MINIDUMP_MEMORY_DESCRIPTOR},
        {'name': 'ThreadContext', 'type': MINIDUMP_LOCATION_DESCRIPTOR},
    ]


class MINIDUMP_THREAD_LIST(VStruct):
    FIELDS = [
        {'name': 'NumberOfThreads', 'type': I4},
        {'name': 'Threads', 'count': DynExpr('$NumberOfThreads'), 'type': MINIDUMP_THREAD},
    ]


class MINIDUMP_MODULE(VStruct):
    FIELDS = [
        {'name': 'BaseOfImage', 'type': I8},
        {'name': 'SizeOfImage', 'type': I4},
        {'name': 'CheckSum', 'type': I4},
        {'name': 'TimeDateStamp', 'type': I4},
        {'name': 'ModuleNameRva', 'type': I4},
        {'name': 'ModuleName', 'flags': 2, 'type': MINIDUMP_STRING, 'dyn_offset': '$ModuleNameRva'},
        {'name': 'VersionInfo', 'type': VS_FIXEDFILEINFO},
        {'name': 'CvRecord', 'type': MINIDUMP_LOCATION_DESCRIPTOR},
        {'name': 'MiscRecord', 'type': MINIDUMP_LOCATION_DESCRIPTOR},
        {'name': 'Reserved0', 'flags': 1, 'type': I8},
        {'name': 'Reserved1', 'flags': 1, 'type': I8},
    ]


class MINIDUMP_MODULE_LIST(VStruct):
    FIELDS = [
        {'name': 'NumberOfModules', 'type': I4},
        {'name': 'Modules', 'count': DynExpr('$NumberOfModules'), 'type': MINIDUMP_MODULE},
    ]


class MINIDUMP_UNLOADED_MODULE(VStruct):
    FIELDS = [
        {'name': 'BaseOfImage', 'type': I8},
        {'name': 'SizeOfImage', 'type': I4},
        {'name': 'CheckSum', 'type': I4},
        {'name': 'TimeDateStamp', 'type': I4},
        {'name': 'ModuleNameRva', 'type': I4},
        {'name': 'ModuleName', 'flags': 2, 'type': MINIDUMP_STRING, 'dyn_offset': '$ModuleNameRva'},
    ]


class MINIDUMP_UNLOADED_MODULE_LIST(VStruct):
    FIELDS = [
        {'name': 'SizeOfHeader', 'type': I4},
        {'name': 'SizeOfEntry', 'type': I4},
        {'name': 'NumberOfEntries', 'type': I4},
        {'name': 'Modules', 'count': DynExpr('$NumberOfEntries'), 'type': MINIDUMP_UNLOADED_MODULE},
    ]


class _MINIDUMP_MEM_RANGES(VStruct):
    FIELDS = [
        {'name': 'Descriptors', 'count': DynExpr('$^NumberOfMemoryRanges'), 'type': MINIDUMP_MEMORY_DESCRIPTOR64},
    ]


class MINIDUMP_MEMORY64_LIST(VStruct):
    FIELDS = [
        {'name': 'NumberOfMemoryRanges', 'type': I8},
        {'name': 'BaseRva', 'type': I8},
        {'name': 'MemoryRanges', 'flags': 2, 'type': _MINIDUMP_MEM_RANGES, 'dyn_offset': '$BaseRva'},
    ]


class MINIDUMP_MEMORY_INFO(VStruct):
    SFORMAT = 'QQIIQIIII'
    SOFFSETS = [0, 8, 16, 20, 24, 32, 36, 40, 44, 48]
    ENDIANNESS = '<'
    FIELDS = [
        {'name': 'BaseAddress', 'type': I8},
        {'name': 'AllocationBase', 'type': I8},
        {'name': 'AllocationProtect', 'type': I4},
        {'name': '__alignment1', 'flags': 1, 'type': I4},
        {'name': 'RegionSize', 'type': I8},
        {'name': 'State', 'type': I4},
        {'name': 'Protect', 'type': I4},
        {'name': 'Type', 'type': I4},
        {'name': '__alignment2', 'flags': 1, 'type': I4},
    ]


class MINIDUMP_MEMORY_INFO_LIST(VStruct):
    FIELDS = [
        {'name': 'SizeOfHeader', 'type': I4},
        {'name': 'SizeOfEntry', 'type': I4},
        {'name': 'NumberOfEntries', 'type': I8},
        {'name': 'Entries', 'count': DynExpr('$NumberOfEntries'), 'type': MINIDUMP_MEMORY_INFO},
    ]


class _MINIDUMP_CPU_INFORMATION(VStruct):
    FIELDS = [
        {'name': 'VendorId', 'type': R, 'length': 12, 'description': 'CPUID subfunction 0'},
        {'name': 'VersionInformation', 'type': I4, 'description': 'CPUID subfunction 1 / EAX'},
        {'name': 'FeatureInformation', 'type': I4, 'description': 'CPUID subfunction 1 / EDX'},
        {'name': 'AMDExtendedCpuFeatures', 'type': I4, 'description': 'CPUID subfunction 80000001 / EBX'},
        {'name': 'ProcessorFeatures', 'count': 2, 'type': I8},
    ]


class MINIDUMP_SYSTEM_INFO(VStruct):
    FIELDS = [
        {'name': 'ProcessorArchitecture', 'type': I2, 'formatter': {9: 'AMD64', 5: 'ARM', 6: 'IA64', 0: 'Intel (x86)', 0xFFFF: 'Unknown'}},
        {'name': 'ProcessorLevel', 'type': I2},
        {'name': 'ProcessorRevision', 'type': I2},
        {'name': 'NumberOfProcessors', 'type': I1},
        {'name': 'ProductType', 'type': I1, 'formatter': {2: 'DOMAIN_CONTROLLER', 3: 'SERVER', 1: 'WORKSTATION'}},
        {'name': 'MajorVersion', 'type': I4},
        {'name': 'MinorVersion', 'type': I4},
        {'name': 'BuildNumber', 'type': I4},
        {'name': 'PlatformId', 'type': I4},
        {'name': 'CSDVersionRva', 'type': I4},
        {'name': 'ServicePack', 'flags': 2, 'type': MINIDUMP_STRING, 'dyn_offset': '$CSDVersionRva'},
        {'name': 'SuiteMask', 'type': I2},
        {'name': 'Reserved2', 'type': I2},
        {'name': 'Cpu', 'type': _MINIDUMP_CPU_INFORMATION},
    ]


class MINIDUMP_MISC_INFO(VStruct):
    SFORMAT = 'IIIIII'
    SOFFSETS = [0, 4, 8, 12, 16, 20, 24]
    ENDIANNESS = '<'
    FIELDS = [
        {'name': 'SizeOfInfo', 'type': I4},
        {'name': 'Flags1', 'type': I4},
        {'name': 'ProcessId', 'type': I4},
        {'name': 'ProcessCreateTime', 'type': I4},
        {'name': 'ProcessUserTime', 'type': I4},
        {'name': 'ProcessKernelTime', 'type': I4},
    ]


class MINIDUMP_HANDLE_DESCRIPTOR_2(VStruct):
    FIELDS = [
        {'name': 'Handle', 'type': I8},
        {'name': 'TypeNameRva', 'type': I4},
        {'name': 'TypeName', 'flags': 2, 'condition': DynExpr('$TypeNameRva'), 'type': MINIDUMP_STRING, 'dyn_offset': '$TypeNameRva'},
        {'name': 'ObjectNameRva', 'type': I4},
        {'name': 'ObjectName', 'flags': 2, 'condition': DynExpr('$ObjectNameRva'), 'type': MINIDUMP_STRING, 'dyn_offset': '$ObjectNameRva'},
        {'name': 'Attributes', 'type': I4},
        {'name': 'GrantedAccess', 'type': I4},
        {'name': 'HandleCount', 'type': I4},
        {'name': 'PointerCount', 'type': I4},
        {'name': 'ObjectInfoRva', 'type': I4},
        {'name': 'Reserved0', 'type': I4},
    ]


class MINIDUMP_HANDLE_DESCRIPTOR(VStruct):
    FIELDS = [
        {'name': 'Handle', 'type': I8},
        {'name': 'TypeNameRva', 'type': I4},
        {'name': 'TypeName', 'flags': 2, 'condition': DynExpr('$TypeNameRva'), 'type': MINIDUMP_STRING, 'dyn_offset': '$TypeNameRva'},
        {'name': 'ObjectNameRva', 'type': I4},
        {'name': 'ObjectName', 'flags': 2, 'condition': DynExpr('$ObjectNameRva'), 'type': MINIDUMP_STRING, 'dyn_offset': '$ObjectNameRva'},
        {'name': 'Attributes', 'type': I4},
        {'name': 'GrantedAccess', 'type': I4},
        {'name': 'HandleCount', 'type': I4},
        {'name': 'PointerCount', 'type': I4},
    ]


class MINIDUMP_HANDLE_DATA_STREAM(VStruct):
    FIELDS = [
        {'name': 'SizeOfHeader', 'type': I4},
        {'name': 'SizeOfDescriptor', 'type': I4},
        {'name': 'NumberOfDescriptors', 'type': I4},
        {'name': 'Reserved', 'type': I4},
        {'name': 'Descriptors', 'count': DynExpr('$NumberOfDescriptors'), 'type': MINIDUMP_HANDLE_DESCRIPTOR, 'condition': '$SizeOfDescriptor == 40'},
        {'name': 'Descriptors2', 'count': DynExpr('$NumberOfDescriptors'), 'type': MINIDUMP_HANDLE_DESCRIPTOR_2, 'condition': '$SizeOfDescriptor != 40'},
    ]


class MINIDUMP_DIRECTORY_ENTRY(VStruct):
    FIELDS = [
        {'name': 'StreamType', 'type': I4},
        {'name': 'Location', 'type': MINIDUMP_LOCATION_DESCRIPTOR},
        {'name': 'Threads', 'flags': 2, 'condition': DynExpr('$StreamType == 3'), 'type': MINIDUMP_THREAD_LIST, 'dyn_offset': '$Location.Rva'},
        {'name': 'ThreadInfo', 'flags': 2, 'condition': DynExpr('$StreamType == 17'), 'type': MINIDUMP_THREAD_INFO_LIST, 'dyn_offset': '$Location.Rva'},
        {'name': 'Modules', 'flags': 2, 'condition': DynExpr('$StreamType == 4'), 'type': MINIDUMP_MODULE_LIST, 'dyn_offset': '$Location.Rva'},
        {'name': 'UnloadedModules', 'flags': 2, 'condition': DynExpr('$StreamType == 14'), 'type': MINIDUMP_UNLOADED_MODULE_LIST, 'dyn_offset': '$Location.Rva'},
        {'name': 'MemoryRanges64', 'flags': 2, 'condition': DynExpr('$StreamType == 9'), 'type': MINIDUMP_MEMORY64_LIST, 'dyn_offset': '$Location.Rva'},
        {'name': 'MemoryInfo', 'flags': 2, 'condition': DynExpr('$StreamType == 16'), 'type': MINIDUMP_MEMORY_INFO_LIST, 'dyn_offset': '$Location.Rva'},
        {'name': 'MemoryInfo', 'flags': 2, 'condition': DynExpr('$StreamType == 7'), 'type': MINIDUMP_SYSTEM_INFO, 'dyn_offset': '$Location.Rva'},
        {'name': 'MemoryInfo', 'flags': 2, 'condition': DynExpr('$StreamType == 15'), 'type': MINIDUMP_MISC_INFO, 'dyn_offset': '$Location.Rva'},
        {'name': 'MemoryInfo', 'flags': 2, 'condition': DynExpr('$StreamType == 12'), 'type': MINIDUMP_HANDLE_DATA_STREAM, 'dyn_offset': '$Location.Rva'},
    ]


class MINIDUMP_DIRECTORY(VStruct):
    FIELDS = [
        {'name': 'Entries', 'count': DynExpr('$^Header.NumberOfStreams'), 'type': MINIDUMP_DIRECTORY_ENTRY},
    ]


class MINIDUMP_HEADER(VStruct):
    FIELDS = [
        {'name': 'Signature', 'type': R, 'length': 4},
        {'name': 'Version', 'type': I4},
        {'name': 'NumberOfStreams', 'type': I4},
        {'name': 'StreamDirectoryRva', 'type': I4},
        {'name': 'CheckSum', 'type': I4},
        {'name': 'TimeDateStamp', 'type': I4UT},
        {'name': 'Flags', 'type': BM8, 'bit_map': {1: 'WithDataSegs', 2: 'WithFullMemory', 4: 'WithHandleData', 8: 'FilterMemory', 16: 'ScanMemory', 32: 'WithUnloadedModules', 64: 'WithIndirectlyReferencedMemory', 128: 'FilterModulePaths', 256: 'ProcessThreadData', 512: 'WithPrivateReadWriteMemory', 1024: 'WithoutOptionalData', 2048: 'FullMemoryInfo', 4096: 'WithThreadInfo', 8192: 'WithCodeSegs', 16384: 'WithoutAuxiliaryState', 32768: 'WithFullAuxiliaryState', 65536: 'WithPrivateWriteCopyMemory', 131072: 'IgnoreInaccessibleMemory', 262144: 'WithTokenInformation', 524288: 'WithModuleHeaders', 1048576: 'FilterTriage', 2097152: 'WithAvxXStateContext', 4194304: 'WithIptTrace', 8388608: 'ScanInaccessiblePartialPages'}},
    ]


class minidump(VStruct):
    ROOT = True
    FILETYPES = ('mdmp',)
    FILTER = '^MDMP'
    FIELDS = [
        {'name': 'Header', 'type': MINIDUMP_HEADER},
        {'name': 'Directory', 'flags': 2, 'type': MINIDUMP_DIRECTORY, 'dyn_offset': '$Header.StreamDirectoryRva'},
    ]


