# automatically generated from MBR.vs2

from libvstruct2 import AS, B, BM1, D, DynExpr, Eval, I1, I2, I4, R, VStruct, W


MBR_PARTITION_TYPES = {
    0x00:'N/A',
    0x01:'12-bit FAT',
    0x02:'XENIX root file system',
    0x03:'XENIX /usr file system (obsolete)',
    0x04:'16-bit FAT, partition <32 MB',
    0x05:'Extended partition',
    0x06:'16-bit FAT, partition >=32 MB',
    0x07:'NTFS / HPFS / QNX / Advanced Unix',
    0x08:'AIX (Linux) / SplitDrive / OS/2 / Dell array',
    0x09:'AIX / Coherent file system',
    0x09:'QNX',
    0x0A:'Coherent swap partition / OPUS / OS/2 Boot Manager',
    0x0B:'32-bit FAT',
    0x0C:'32-bit FAT (Ext. INT 13)',
    0x0E:'16-bit FAT >= 32 MB (Ext. INT 13)',
    0x0F:'Extended partition (Ext. INT 13)',
    0x10:'OPUS',
    0x11:'Hidden 12-bit FAT',
    0x12:'Compaq diagnostics (Landis)',
    0x14:'Hidden 16-bit FAT, partition <32 MB',
    0x16:'Hidden 16-bit FAT, partition >= 32 MB',
    0x17:'Hidden IFS',
    0x18:'AST Windows swap file',
    0x19:'Willowtech Photon coS',
    0x1B:'Hidden 32-bit FAT',
    0x1C:'Hidden 32-bit FAT (Ext. INT 13)',
    0x1E:'Hidden 16-bit FAT >32 MB, Ext INT 13 (PowerQuest specific)',
    0x20:'Willowsoft Overture File System (OFS1)',
    0x21:'Oxygen FSo2',
    0x22:'Oxygen Extended',
    0x23:'officially listed as reserved (HP Volume Expansion, SpeedStor variant?)',
    0x24:'NEC MS-DOS 3.x',
    0x26:'officially listed as reserved (HP Volume Expansion, SpeedStor variant?)',
    0x31:'officially listed as reserved (HP Volume Expansion, SpeedStor variant?)',
    0x33:'officially listed as reserved (HP Volume Expansion, SpeedStor variant?)',
    0x34:'officially listed as reserved (HP Volume Expansion, SpeedStor variant?)',
    0x36:'officially listed as reserved (HP Volume Expansion, SpeedStor variant?)',
    0x38:'Theos',
    0x3C:'PowerQuest Files Partition Format',
    0x3D:'Hidden NetWare',
    0x40:'VENIX 80286',
    0x41:'Personal RISC Boot (Landis) / PowerPC boot partition / PTS-DOS 6.70',
    0x42:'Windows 2000 (NT 5): Dynamic extended partition',
    0x43:'Alternative Linux native file system (EXT2fs)',
    0x43:'PTS-DOS 6.70 & BootWizard: DR-DOS',
    0x45:'Priam / EUMEL/Elan',
    0x46:'EUMEL/Elan',
    0x47:'EUMEL/Elan',
    0x48:'EUMEL/Elan',
    0x4A:'ALFS/THIN lightweight filesystem for DOS',
    0x4D:'QNX',
    0x4E:'QNX',
    0x4F:'QNX / Oberon boot/data partition',
    0x50:'Ontrack Disk Manager, read-only partition, FAT partition (Logical sector size varies)',
    0x51:'Ontrack Disk Manager, read/write partition / Novell',
    0x52:'CP/M / Microport System V/386',
    0x53:'Ontrack Disk Manager, write-only (Landis)',
    0x54:'Ontrack Disk Manager 6.0 (DDO)',
    0x55:'EZ-Drive 3.05',
    0x56:'Golden Bow VFeature',
    0x5C:'Priam EDISK',
    0x61:'Storage Dimensions SpeedStor',
    0x63:'GNU HURD / Mach, MtXinu BSD 4.2 on Mach / Unix Sys V/386, 386/ix',
    0x64:'Novell NetWare 286 / SpeedStore (Landis)',
    0x65:'Novell NetWare (3.11 and 4.1)',
    0x66:'Novell NetWare 386',
    0x67:'Novell NetWare',
    0x68:'Novell NetWare',
    0x69:'Novell NetWare 5+; Novell Storage Services (NSS)',
    0x70:'DiskSecure Multi-Boot',
    0x75:'IBM PC/IX',
    0x80:'Minix v1.1 - 1.4a / Old MINIX (Linux)',
    0x81:'Linux/Minix v1.4b+ / Mitac Advanced Disk Manager',
    0x82:'Linux Swap partition / Prime (Landis) / Solaris (Unix)',
    0x83:'Linux native file system (EXT2fs/xiafs)',
    0x84:'OS/2 hiding type 04h partition / APM hibernation, can be used by Win98',
    0x86:'NT Stripe Set, Volume Set?',
    0x87:'HPFS FT mirrored partition (Landis)',
    0x8E:'Linux Logical Volume Manager partition',
    0x93:'Amoeba file system  / Hidden Linux EXT2 partition',
    0x94:'Amoeba bad block table',
    0x99:'Mylex EISA SCSI',
    0x9F:'BSDI',
    0xA0:'Phoenix NoteBios Power Management "Save to Disk" / IBM hibernation',
    0xA1:'HP Volume Expansion (SpeedStor variant)',
    0xA3:'HP Volume Expansion (SpeedStor variant)',
    0xA4:'HP Volume Expansion (SpeedStor variant)',
    0xA5:'FreeBSD/386',
    0xA6:'OpenBSD / HP Volume Expansion (SpeedStor variant)',
    0xA7:'NextStep Partition',
    0xA9:'NetBSD',
    0xAA:'Olivetti DOS with FAT12',
    0xB0:'     part of Bootmanager BootStar by Star-Tools GmbH',
    0xB1:'HP Volume Expansion (SpeedStor variant)',
    0xB3:'HP Volume Expansion (SpeedStor variant)',
    0xB4:'HP Volume Expansion (SpeedStor variant)',
    0xB6:'HP Volume Expansion (SpeedStor variant)',
    0xB7:'BSDI file system or secondarily swap',
    0xB8:'BSDI swap partition or secondarily file system',
    0xBB:'PTS BootWizard',
    0xBE:'Solaris boot partition',
    0xC0:'Novell DOS/OpenDOS/DR-OpenDOS/DR-DOS secured partition / CTOS (reported by a customer)',
    0xC1:'DR-DOS 6.0 LOGIN.EXE-secured 12-bit FAT partition',
    0xC2:'Reserved for DR-DOS 7+',
    0xC3:'Reserved for DR-DOS 7+',
    0xC4:'DR-DOS 6.0 LOGIN.EXE-secured 16-bit FAT partition',
    0xC6:'DR-DOS 6.0 LOGIN.EXE-secured Huge partition / corrupted FAT16 volume/stripe (V/S) set (Windows NT)',
    0xC7:'Syrinx / Cyrnix (Landis) / HPFS FT disabled mirrored partition / corrupted NTFS volume/stripe set',
    0xC8:'Reserved for DR-DOS 7+',
    0xC9:'Reserved for DR-DOS 7+',
    0xCA:'Reserved for DR-DOS 7+',
    0xCB:'Reserved for DR-DOS secured FAT32',
    0xCC:'Reserved for DR-DOS secured FAT32X (LBA)',
    0xCD:'Reserved for DR-DOS 7+',
    0xCE:'Reserved for DR-DOS secured FAT16X (LBA)',
    0xCF:'Reserved for DR-DOS secured extended partition (LBA)',
    0xD0:'Multiuser DOS secured (FAT12???)',
    0xD1:'Old Multiuser DOS secured FAT12',
    0xD4:'Old Multiuser DOS secured FAT16 (<= 32M)',
    0xD5:'Old Multiuser DOS secured extended partition',
    0xD6:'Old Multiuser DOS secured FAT16 (BIGDOS > 32 Mb)',
    0xD8:'CP/M 86',
    0xDB:'CTOS (Convergent Technologies OS)',
    0xDE:'DeLL partition',
    0xDF:'BootIt EMBRM',
    0xE1:'SpeedStor 12-bit FAT extended partition / DOS access (Linux)',
    0xE2:'DOS read-only (Florian Painke\'s XFDISK 1.0.4)',
    0xE3:'SpeedStor (Norton, Linux says DOS R/O)',
    0xE4:'SpeedStor 16-bit FAT extended partition',
    0xE5:'Tandy DOS with logical sectored FAT',
    0xE6:'Storage Dimensions SpeedStor',
    0xEB:'BeOS file system',
    0xED:'Reserved for Matthias Paul\'s Spryt*x',
    0xEE:'GPT protective MBR',
    0xF1:'SpeedStor Dimensions (Norton,Landis)',
    0xF2:'DOS 3.3+ second partition',
    0xF2:'Unisys DOS with logical sectored FAT',
    0xF3:'Storage Dimensions SpeedStor',
    0xF4:'SpeedStor Storage Dimensions (Norton,Landis)',
    0xF5:'Prologue',
    0xF6:'Storage Dimensions SpeedStor',
    0xFD:'Reserved for FreeDOS (http://www.freedos.org)',
    0xFE:'LANstep / IBM PS/2 IML  / Storage Dimensions SpeedStor',
    0xFF:'Xenix bad-block table',
}

MBR_MEDIA_TYPE = {
    0xF0: 'FLOPPY',
    0xF8: 'HARD_DRIVE',
    0xFA: 'FLOPPY_320K',
    0xFB: 'FLOPPY_640K',
    0xFC: 'FLOPPY_180K',
    0xFD: 'FLOPPY_360K',
    0xFE: 'FLOPPY_160K',
    0xFF: 'FLOPPY_320K',
}


class CHSAddr(VStruct):
    FIELDS = [
        {'name': 'Head', 'type': B},
        {'name': 'Raw1', 'flags': 1, 'type': B},
        {'name': 'Raw2', 'flags': 1, 'type': B},
        {'name': 'Sector', 'flags': 2, 'expression': '$Raw1 & 0x3F', 'type': Eval},
        {'name': 'Cylinder', 'flags': 2, 'expression': '$Raw2 + (($Raw1 << 2) & 0x300)', 'type': Eval},
    ]


class MBR_PartRecord(VStruct):
    FIELDS = [
        {'name': 'Status', 'type': BM1, 'bit_map': {128: 'Bootable'}},
        {'name': 'FirstSectCHS', 'type': CHSAddr},
        {'name': 'PartType', 'type': B, 'formatter': MBR_PARTITION_TYPES},
        {'name': 'LastSectCHS', 'type': CHSAddr},
        {'name': 'FirstSectLBA', 'type': D},
        {'name': 'SectCnt', 'type': D},
    ]


class DOS_BootSector(VStruct):
    FIELDS = [
        {'name': 'Jump', 'type': R, 'length': 3},
        {'name': 'OemName', 'type': R, 'length': 8},
        {'name': 'BytesPerSector', 'type': I2},
        {'name': 'SectorsPerCluster', 'type': I1},
        {'name': 'ReservedSectors', 'type': I2},
        {'name': 'NumFATTables', 'type': I1},
        {'name': 'MaxRootEntries', 'type': I2},
        {'name': 'NumSectors16', 'type': I2},
        {'name': 'MediaDescriptor', 'type': I1, 'formatter': MBR_MEDIA_TYPE},
        {'name': 'SectorsPerFAT', 'type': I2},
        {'name': 'SectorsPerTrack', 'type': I2},
        {'name': 'HeadsPerCylinder', 'type': I2},
        {'name': 'NumHiddenSectors', 'type': I4},
        {'name': 'NumSectors32', 'type': I4},
        {'name': 'DriveNum', 'type': I1},
        {'name': 'Reserved', 'flags': 1, 'type': I1},
        {'name': 'BootSig', 'type': I1},
        {'name': 'UniqueId', 'type': I4},
        {'name': 'VolumeLabel', 'type': AS, 'length': 11},
        {'name': 'FSLabel', 'type': AS, 'length': 8},
        {'name': 'Code', 'type': R, 'length': 400},
    ]


class MBR(VStruct):
    ROOT = True
    FILETYPES = ('mbr',)
    FILTER = '^.{510}\\x55\\xAA'
    FIELDS = [
        {'name': 'BootstrapCodeArea', 'type': R, 'length': 440},
        {'name': 'BootSector', 'flags': 2, 'condition': DynExpr("&BootstrapCodeArea.value[3: 8] == b'MSDOS'"), 'type': DOS_BootSector, 'dyn_offset': '@BootstrapCodeArea'},
        {'name': 'Signature', 'type': AS, 'length': 4},
        {'name': 'Unused', 'type': W},
        {'name': 'PartTable', 'count': 4, 'type': MBR_PartRecord},
        {'name': 'MBRSignature', 'type': W, 'validator': 43605},
    ]


