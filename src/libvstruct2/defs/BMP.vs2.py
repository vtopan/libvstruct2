BMP_COMPRESSION = {
    0:'BI_RGB',
    1:'BI_RLE8',
    2:'BI_RLE4',
    3:'BI_BITFIELDS',
    4:'BI_JPEG',
    5:'BI_PNG',
    6:'BI_ALPHABITFIELDS',
    }
