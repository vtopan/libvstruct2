E820_MEM_TYPE = {
    0x0001: 'Available',
    0x0002: 'Reserved',
    0x0003: 'ACPI-reclaimable',
    0x0004: 'ACPI-NVS',
    0x0005: 'Unusable',
    0x0006: 'Disabled',
    }
